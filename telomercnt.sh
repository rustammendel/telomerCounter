#!/bin/bash
if [[ -z $1 ]]
then
	echo "Please input chromosome number: "
	read chromoNum
else
	chromoNum=$1
fi

filename=hs*$chromoNum.fa

wget ftp://ftp.ncbi.nlm.nih.gov/genomes/Homo_sapiens/CHR_$chromoNum/$filename.gz
gunzip $filename.gz

newfn=human_$chromoNum.fasta
mv $filename $newfn

if [[ -z $2 ]]
then
	firstNlines=125 #Change this nubmer to desired number of lines
else
	firstNlines=$2
fi
 
head -n $firstNlines $newfn | grep -o TTAGGG | wc -l

#Uncomment the line below to count telomers in last N lines: 
#tail -n $firstNlines $newfn | grep -o TTAGGG | wc -l
