# telomerCounter
 The bash script which intended for counting telomeres in DNA sequence of homo sapiens
 
Name:
 telomercnt - print number of telomeres in DNA sequence

Synopsis: 
 telomercnt CHROMOSOME LINES

Description:
 telomercnt downloads CHROMOSOME of human, unzips and converts it to .fasta, searches telomeres (TTAGGG) in choosen first n LINES.
 If no CHROSOME number is given as parameter, script will ask for user input, If no LINES is given as parameter, default 125 lines      will be appiled.
 Download source: ftp://ftp.ncbi.nlm.nih.gov/genomes/Homo_sapiens/
 
Example: 
 ./telomercnt.sh 20 200
 output: 2
